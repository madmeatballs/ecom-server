const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Product name is required']
    },
    description: {
        type: String,
    },
    quantity: {
        type: Number,
        required: [true, 'Product quantity is required']
    },
    price: {
        type: Number,
        required: [true, 'Product price is required']
    },
    isActive : {
        type: Boolean,
        default: true
    },
    status: {
        type: String,
        default: 'Available'
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

module.exports = mongoose.model('product', productSchema);