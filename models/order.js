const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    product_id: {
        type: String,
        required: [true, 'Order product id is required']
    },
    user_id: {
        type: String,
        required: [true, 'Order user id is required']
    },
    price: {
        type: Number,
        required: [true, 'Order price is required']
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

module.exports = mongoose.model('order', orderSchema);