const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    first_name: {
        type: String,
        required: [true, 'First Name is required']
    },
    last_name: {
        type: String,
        required: [true, 'Last Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },    
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    mobile_number: {
        type: String,
        required: [true, 'Mobile Number is required']
    },
    address: {
        type: String
    },
    status: {
        type: String
    },    
    isAdmin: {
        type: Boolean,
        default: false
    }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

module.exports = mongoose.model('user', userSchema);