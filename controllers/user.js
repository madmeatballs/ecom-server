const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.register = (params) => {
	let user = new User({
		first_name: params.first_name,
		last_name: params.last_name,
		email: params.email,
		mobile_number: params.mobile_number,
		address: params.address,
		password: bcrypt.hashSync(params.password, 10)	
	})
	return user.save().then((user,err) => {
		return (err) ? false : true
	})
}

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		return { email: user.email }
	})
}

module.exports.login = (params) => {
    const {email, password} = params
    return User.findOne({email}).then( user => {

      if(!user) return false
      let isPasswordMatched = bcrypt.compareSync(password, user.password)

      if(!isPasswordMatched) return false
      let accessToken = auth.createAccessToken(user)
      
      return {
        accessToken: accessToken
      }
    }) 
  }