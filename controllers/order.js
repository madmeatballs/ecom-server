const Order = require('../models/order');

module.exports.add = (params) => {
    let order = new Order ({
        product_id: params.product_id,
        user_id: params.user_id,
        price: params.price
    })
    return order.save().then((order, err) => {
        return(err) ? false : true;
    })
}

module.exports.getAll = () => {
    return Order.find({}).then(orders => orders);
}