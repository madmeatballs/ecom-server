const Product = require('../models/product');

module.exports.add = (params) => {
    let product = new Product ({
        name: params.name,
        description: params.description,
        quantity: params.quantity,
        price: params.price
    })
    return product.save().then((product, err) => {
        return(err) ? false : true;
    })
}

module.exports.productExists = (params) => {
    return Product.find({name: new RegExp('^'+params.name.trim()+'$','i')}).then(result => {
        return result.length > 0 ? true : false;
    })
}

module.exports.getAll = () => {
    return Product.find({status: 'Available'}).then(products => products);
}

module.exports.getAlldisabled = () => {
    return Product.find({status: 'Unavailable'}).then(products => products);
}

//this will display a specific product
module.exports.get = (params) => {
    return Product.findById(params.productId).then(product => product);
}

module.exports.edit = (id, data) => {

    return Product.findByIdAndUpdate(
        { _id: id}, data, {useFindAndModify: false}).then((product, err) => {
        return(err) ? false : true;
    });
}

module.exports.delete = (id, data) => {
    return Product.findByIdAndDelete({_id: id}, data).then((err) => {
        return(err) ? false : true;
    });
}

module.exports.archive = (params) => {
    const update = {isActive: false, status: 'Unavailable'}
    return Product.findByIdAndUpdate(params.productId, update).then((doc, err) => {
        return(err) ? false : true;
    });
}

module.exports.enable = (params) => {
    const update = {isActive: true, status: 'Available'}
    return Product.findByIdAndUpdate(params.productId, update).then((doc, err) => {
        return(err) ? false : true;
    });
}