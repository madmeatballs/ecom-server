const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

app.use(cors());

mongoose.connect(connectionString,
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    console.log("Successfully hosted online!")
	res.send("Successfully hosted online")
})

const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');

app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);

let db = mongoose.connection;

db.on('error',console.error.bind(console, 'connection error:'));
db.once('open', () => console.log("Now connected to cloud database: MongoDB Atlas"));
app.listen(port, () => console.log(`React ecomm server up and running on port ${port}`));