const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/product');
const auth = require('../auth');

router.post('/add', (req, res) => {
    ProductController.add(req.body).then(result => res.send(result));
})

router.get('/', (req, res) => {
    ProductController.getAll().then(products => res.send(products));
})

router.get('/:id', (req, res) => {
    const productId = req.params.id //to avoid going back to controllers to check
    ProductController.get({productId}).then(product => res.send(product));
});

router.post('/product-exists', (req, res) => {
    ProductController.productExists(req.body).then(result => res.send(result));
});

router.patch('/edi/:id', (req, res) => {
    // console.log(req.body, req.params);
    ProductController.edit(req.params.id, req.body).then(result => res.send(result));
});

router.delete('/del/:id', auth.verify, (req, res) => {
    ProductController.delete(req.params.id).then(result => res.send(result));
});

//disable a product
router.patch('/dis/:id', auth.verify, (req, res) => {
    const productId = req.params.id;
    ProductController.archive({productId}).then(result => res.send(result));
});

//re-enable a product
router.patch('/ena/:id', auth.verify, (req, res) => {
    const productId = req.params.id;
    ProductController.enable({productId}).then(result => res.send(result));
});


module.exports = router;