const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/order');
const auth = require('../auth');

router.get('/', (req, res) => {
    OrderController.getAll().then(orders => res.send(orders));
})

router.post('/add', (req, res) => {
    OrderController.add(req.body).then(result => res.send(result));
})

module.exports = router;